Architecture of my framework.
 My framework is classified into six components.
 1. test driving mechanism,
 2. test scripts,
 3. common function are divided into two parts
	 a.API related common function,
	 b. utility common function, 
 4. data files/variable files.
 5. libraries,
 6. reports.

 In my framework I am using static driver class and dynamic driver class to run my framework.
 In static driver class, I am directly importing my test script into my main method to execute them.
 As name suggest it is static in nature thus I have to explicitly write the test scripts over there.
 But when I am using dynamic class mechanism, in that case by using Java.lang.reflect,
 I can dynamically call the test scripts from my test script package and execute them.

 Then moving to the test script package.
 In this test script package, I have all the test cases specified test scripts for each API.
 I have multiple test cases, For each test cases I have one single test script.

 In common functions I have divided it into two categories.
 1.API related common function.
	 These are the common function which are used to trigger the API,
	 fetch the status code,
	 fetch the response body.
 Next,
 I have utility related common functions which are creating a API related logs in text file.
	 That means once API is executed its corresponding endpoint request body, request body,
	 response body and headers are saved in separate notepad or text file for each test scripts.
 And next utility is,
 I have one utility. It will create the directory for test execution automatically if directory doesn't exist 
		and if directory exist then it will delete existing one and then it will create a new file.

 Third utility I have is to read the data files from excel workbook.
 This completes my common functions.

 After that we are reading data from the excel file by using Apache POI library.
 I have created utility for the same.

 Next the libraries which I am using into my framework are:
 1. rest assured: to trigger the API, extract the response code, extract the response body, parse the for response body by using JSON path. 
 2. test Ng library: So by using Test Ng library I am validating the response body parameters by using assert. 
 3. Apache POI library: it is used to read and write from the excel file.

  Then I have reporting mechanism in the reports.
 I am using extend reports currently.
 Earlier I was using earlier report as well but due to its problem related to caching we have started using extend report.

 That's all about my framework.