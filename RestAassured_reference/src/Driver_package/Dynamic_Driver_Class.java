package Driver_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.testng.annotations.Test;

import utility_common_methods.excel_data_extractor;

public class Dynamic_Driver_Class {
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_execute = excel_data_extractor.Excel_data_reader("test_data", "Test_cases", "TC_name");
		System.out.println(TC_execute);
		int count = TC_execute.size();
		for (int i = 1; i < count; i++) {
			String TC_name = TC_execute.get(i);
			System.out.println(TC_name);
			// Call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> Test_class = Class.forName("Test_package." + TC_name);

			// Call the execute method belonging to test class captured in variable TC_name
			// by using java.lang.reflect.method class
			Method execute_method = Test_class.getDeclaredMethod("executor");

			// Set the accessibility of method true
			execute_method.setAccessible(true);

			// Create the instance of testclass captured in variable name testclassname
			Object instance_of_class = Test_class.getDeclaredConstructor().newInstance();

			// Execute the test script class fetched in variable Test_class
			execute_method.invoke(instance_of_class);

		}
	}

}
