package Test_package;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.common_method_handle_API;
import endpoint_repository.get_endpoint_repository;
import utility_common_methods.handle_api_logs;
import utility_common_methods.handle_directory;

public class get_TC1 extends common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir=handle_directory.create_log_directory("get_TC1_logs");
		String endpoint = get_endpoint_repository.get_request();
		for (int i = 0; i < 5; i++) {
			int statusCode = get_statusCode(endpoint);
			System.out.println("get API triggered");
			System.out.println(statusCode);
			if (statusCode == 200) {
				String responseBody = get_responseBody(endpoint);
				System.out.println(responseBody);
				handle_api_logs.evidence_creator(log_dir, "get_TC1", endpoint, null, responseBody);
				get_TC1.validator(responseBody);
				break;
			} 
			else {
				System.out.println("get API expected status code 200 not found hence re-trying it");
			}
	}
	}
	public static void validator(String responseBody) {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in","byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			int exp_id = expected_id[i];
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String exp_firstname = expected_firstname[i];
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String exp_lastname = expected_lastname[i];
			String res_email = dataarray.getJSONObject(i).getString("email");
			String exp_email = expected_email[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_email);
	}
}
	
}
