package API_common_methods;

import static io.restassured.RestAssured.given;

public class common_method_handle_API {
	public static int post_statusCode(String requestBody, String endpoint) {
		int statuscode=given().header("Content-Type", "application/json").body(requestBody).when()
				.post(endpoint).then().extract().statusCode();
		return statuscode;
	} 
		public static String post_responseBody(String requestBody, String endpoint) {
		String responseBody=given().header("Content-Type", "application/json").body(requestBody).when()
				.post(endpoint).then().extract().response().asString();
		return responseBody;
	} 
 //___________________________________________________________________________________________________________________

		public static int put_statusCode(String requestBody, String endpoint) {
		int statuscode=given().header("Content-Type", "application/json").body(requestBody).when()
				.put(endpoint).then().extract().statusCode();
		return statuscode;
	} 
		public static String put_responseBody(String requestBody, String endpoint) {
		String responseBody=given().header("Content-Type", "application/json").body(requestBody).when()
				.post(endpoint).then().extract().response().asString();
		return responseBody;
	} 
//__________________________________________________________________________________________________________________

		public static int patch_statusCode(String requestBody, String endpoint) {
	
		int statuscode=given().header("Content-Type", "application/json").body(requestBody).when()
				.post(endpoint).then().extract().statusCode();
		return statuscode;
	} 
		public static String patch_responseBody(String requestBody, String endpoint) {
		String responseBody=given().header("Content-Type", "application/json").body(requestBody).when()
				.post(endpoint).then().extract().response().asString();
			return responseBody;
	} 
//____________________________________________________________________________________________________________________
		public static int get_statusCode(String endpoint) {
		int statuscode=given().when()
				.get(endpoint).then().extract().statusCode();
		return statuscode;
	} 
		public static String get_responseBody(String endpoint) {
		String responseBody=given().when()
				.get(endpoint).then().extract().response().asString();
		return responseBody;
	}
}

